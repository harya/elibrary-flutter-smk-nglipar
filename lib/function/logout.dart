import 'package:flutter/material.dart';
import 'package:elibrary_master/screens/splash/splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

void logout(context) async {
  SharedPreferences localStorage = await SharedPreferences.getInstance();
  localStorage.remove('group');
  localStorage.remove('token');
  localStorage.remove('angkatan');
  localStorage.remove('nis');
  localStorage.remove('auth');
  localStorage.remove('foto_siswa');
  localStorage.remove('email');
  localStorage.remove('id_user');
  localStorage.remove('user');
  localStorage.remove('token');
  Navigator.push<void>(
    context,
    MaterialPageRoute<void>(
      builder: (context) => const SplashScreen(),
    ),
  );
}
