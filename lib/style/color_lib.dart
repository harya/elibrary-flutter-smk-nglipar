import 'package:flutter/material.dart';

class ColorLib {
  static const blueAccent = Colors.blueAccent;
  static const blueLight = Colors.lightBlueAccent;
  static const whiteColor = Color(0xffffffff);

  static const dotColor = Color(0xffe8e8e8);
  static const bgStyleOne = Color(0xff4d669b);
  static const bgStyleAppBar = Color.fromARGB(255, 34, 61, 79);
  static const underlineTextField = Color(0xff8b97ff);
  static const hintColor = Color(0xffccd1ff);
  static const textColor01 = Color.fromARGB(255, 30, 33, 55);
  static const textColor02 = Color.fromARGB(171, 122, 123, 133);
  static const colorColl01 = Color.fromARGB(255, 212, 229, 239);
}
