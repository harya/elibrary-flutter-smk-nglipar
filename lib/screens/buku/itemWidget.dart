import 'package:flutter/material.dart';
import 'package:elibrary_master/model/bukusModel.dart';
import 'package:elibrary_master/constant/url.dart';
import 'dart:core';

class ItemWidget extends StatelessWidget {
  final Bukus item;
  const ItemWidget({Key? key, required this.item}) : super(key: key);
  final urlImageBuku = imgBukuUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(1.0),
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Row(children: <Widget>[
        Expanded(
          flex: 3,
          child: Card(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(2.0))),
            child: InkWell(
              onTap: () => print("ciao"),
              customBorder: const CircleBorder(),
              child: Column(
                children: <Widget>[
                  const Padding(padding: EdgeInsets.only(top: 10.0)),
                  ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(18.0),
                      topRight: Radius.circular(18.0),
                    ),
                    child: Image.network('$urlImageBuku/${item.bukuCover}',
                        width: 150, height: 250, fit: BoxFit.cover),
                  ),
                  ListTile(
                    title: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(item.bukuJudul),
                      ),
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(item.penulisNama),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ]),
    );
  }
}
