import 'package:flutter/material.dart';
import 'package:elibrary_master/model/bukusModel.dart';
import 'package:elibrary_master/screens/buku/itemWidget.dart';
import 'package:elibrary_master/services/bukuService.dart';

class BukuPage extends StatefulWidget {
  const BukuPage({super.key});

  @override
  _BukuPageState createState() => _BukuPageState();
}

class _BukuPageState extends State<BukuPage> {
  //List<bukus>? listBuku = [];List<Bukus> bukus;

  Future<List<Bukus>>? _buku;

  @override
  void initState() {
    super.initState();
    _buku = BukuServices.getBuku();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder(
            future: _buku,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Bukus>? bukus = snapshot.data!;
                return ListView.builder(
                    itemCount: bukus.length,
                    itemBuilder: (context, index) {
                      return ItemWidget(item: bukus[index]);
                    });
              } else {
                return Text(
                  '${snapshot.error}',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 16),
                );
              }
            }),
      ),
    );
  }
}
