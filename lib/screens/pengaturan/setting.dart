import 'package:elibrary_master/function/logout.dart';
import 'package:flutter/material.dart';
import 'package:elibrary_master/style/color_lib.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Setting'),
      ),
      body: SafeArea(
          minimum: EdgeInsets.all(20),
          child: Column(
            children: [
              Padding(padding: EdgeInsets.only(top: 5)),
              Row(
                children: [
                  TextButton.icon(
                    onPressed: () => print('data pengguna'), //logout(context),
                    icon: Icon(
                      Icons.account_box,
                      size: 24.0,
                      color: ColorLib.textColor01,
                    ),
                    label: Text('Profil'),
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.only(
                          right: 15, left: 10, top: 10, bottom: 10),
                      primary: ColorLib.textColor01,
                    ),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              Row(
                children: [
                  TextButton.icon(
                    onPressed: () => print('setting'), //logout(context),
                    icon: Icon(
                      Icons.settings,
                      size: 24.0,
                      color: ColorLib.textColor01,
                    ),
                    label: Text('Setting'),
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.only(
                          right: 15, left: 10, top: 10, bottom: 10),
                      primary: ColorLib.textColor01,
                      //backgroundColor: ColorLib.textColor01,
                      //onSurface: Colors.grey,
                      //shape: const BeveledRectangleBorder(
                      //    borderRadius: BorderRadius.all(Radius.circular(3))),
                    ),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              Row(
                children: [
                  TextButton.icon(
                    onPressed: () => logout(context),
                    icon: Icon(
                      Icons.logout,
                      size: 24.0,
                      color: ColorLib.bgStyleAppBar,
                    ),
                    label: Text('Logout'),
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.only(
                          right: 15, left: 10, top: 10, bottom: 10),
                      primary: ColorLib.textColor01,
                      //backgroundColor: ColorLib.textColor01,
                      //onSurface: Colors.grey,
                      //shape: const BeveledRectangleBorder(
                      //    borderRadius: BorderRadius.all(Radius.circular(3))),
                    ),
                  )
                ],
              ),
            ],
          )),
    );
  }
}
