import 'dart:convert';
import 'dart:core';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:elibrary_master/constant/url.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:elibrary_master/services/token.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:no_screenshot/no_screenshot.dart';

class ReadPage extends StatefulWidget {
  ReadPage({super.key, this.bukuId});
  final String? bukuId;

  @override
  _ReadPageState createState() => _ReadPageState();
}

class _ReadPageState extends State<ReadPage> {
  final urlPdfBuku = pdfBukuUrl;
  final _noScreenshot = NoScreenshot.instance;

  String? file_url, nama_file, id_user;
  Uint8List? _documentBytes;
// ======================================================
  //------------------------
  @override
  void initState() {
    //_noScreenshot.screenshotOff();
    getPdfFile();
    super.initState();
  }

//------------------------
  getPdfFile() async {

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    late String? id_user =
        localStorage.getString('id_user').toString().replaceAll('"', '');
    await getToken();
    final fullUrl =
        bukuUrl + widget.bukuId.toString() + '/' + id_user + '/read';

    final response = await http.get(Uri.parse(fullUrl), headers: setHeaders());
    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      String? _file_url = pdfBukuUrl +
          json
              .encode(body['showbuku']['buku_file'])
              .toString()
              .replaceAll('"', '');

      _documentBytes = await http
          .readBytes(Uri.parse('http://' + _file_url.replaceAll('"', '')));
    } else {
      throw jsonDecode(response.body)['message'];
    }
    setState(() {});
  }

  //------------------------
// ======================================================
  @override
  Widget build(BuildContext context) {
    Widget child = const Center(child: CircularProgressIndicator());
    if (_documentBytes != null) {
      child = SfPdfViewer.memory(
        _documentBytes!,
      );
    }
    return Scaffold(
      appBar: AppBar(title: Text('Buku')),
      body: child,
    );
  }
}
