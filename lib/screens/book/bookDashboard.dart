import 'package:elibrary_master/screens/book/bookmain.dart';
import 'package:elibrary_master/screens/book/booksearch.dart';
import 'package:elibrary_master/screens/home/home.dart';
import 'package:elibrary_master/screens/pengaturan/setting.dart';

import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:elibrary_master/style/color_lib.dart';

class BookDashboardPage extends StatefulWidget {
  const BookDashboardPage({super.key, this.bukuJudul});
  final String? bukuJudul;
  @override
  _BookDashboardState createState() => _BookDashboardState();
}

class _BookDashboardState extends State<BookDashboardPage> {
  Widget? _child;
  var bukuJudul;
  @override
  void initState() {
    String? bukuJudul = widget.bukuJudul.toString();
    _child = BookSearch(bukuJudul: bukuJudul);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _child,
      bottomNavigationBar: FluidNavBar(
        icons: [
          FluidNavBarIcon(
              icon: Icons.home,
              backgroundColor: ColorLib.bgStyleAppBar,
              extras: {"label": "Home"}),
          FluidNavBarIcon(
              icon: Icons.book_sharp,
              backgroundColor: ColorLib.bgStyleAppBar,
              extras: {"label": "Buku"}),
          FluidNavBarIcon(
              icon: Icons.settings,
              backgroundColor: ColorLib.bgStyleAppBar,
              extras: {"label": "Setting"}),
        ],
        onChange: _handleNavigationChange,
      ),
    );
  }

  void _handleNavigationChange(int index) {
    setState(() {
      switch (index) {
        case 0:
          _child = const HomePage();
          break;
        case 1:
          _child = const BookMain();
          break;
        case 2:
          _child = const SettingPage();
          break;
      }
      _child = AnimatedSwitcher(
        switchInCurve: Curves.easeOut,
        switchOutCurve: Curves.easeIn,
        duration: const Duration(milliseconds: 500),
        child: _child,
      );
    });
  }
}
