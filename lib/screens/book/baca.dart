import 'package:flutter/material.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

class BacaPage extends StatefulWidget {
  const BacaPage({super.key, this.bukuFile});

  final String? bukuFile;

  @override
  _BacaPageState createState() => _BacaPageState();
}

class _BacaPageState extends State<BacaPage> {
  String urlPDFPath = "";

  @override
  void initState() {
    super.initState();
    getFileFromUrl("Url here").then((f) {
      setState(() {
        urlPDFPath = f.path;
      });
    });
  }

  Future<File> getFileFromUrl(url) async {
    try {
      var data = await http.get(url);
      var bytes = data.bodyBytes;
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/mypdf.pdf");

      File urlFile = await file.writeAsBytes(bytes);
      return urlFile;
    } catch (e) {
      throw Exception("Error loading url pdf");
    }
  }

  @override
  Widget build(BuildContext context) {
    final bukuFile = widget.bukuFile.toString();
    return Scaffold(
      appBar: AppBar(title: Text(bukuFile)),
    );
  }
}
