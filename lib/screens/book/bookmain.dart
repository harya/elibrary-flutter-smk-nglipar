import 'package:elibrary_master/screens/book/bookDashboard.dart';
import 'package:elibrary_master/screens/book/booksearch.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:elibrary_master/model/bookModel.dart';
import 'package:elibrary_master/services/bookService.dart';
import 'package:elibrary_master/constant/url.dart';
import 'package:elibrary_master/screens/book/read.dart';
import 'package:elibrary_master/style/color_lib.dart';

class BookMain extends StatefulWidget {
  const BookMain({super.key});

  @override
  _bookMainState createState() => _bookMainState();
}

class _bookMainState extends State<BookMain> {
  // Declare ============================================================
  bool _isLoading = false;
  bool _secureText = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  String? bukuJudul;
  Future<List<BookModel>>? _bookService;
  final urlImageBuku = imgBukuUrl;
  // Message ============================================================
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg!),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    _bookService = BookService.getBook();
  }

  // Pencarian ==========================================================
  Widget _pencarianBuku() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(24),
          key: _scaffoldKey,
          child: Form(
              key: _formKey,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextFormField(
                      cursorColor: ColorLib.blueLight,
                      keyboardType: TextInputType.text,
                      decoration: const InputDecoration(
                        hintText: "Masukan Judul buku yang dicari",
                      ),
                      validator: (bukuJudulvalue) {
                        if (bukuJudulvalue == '') {
                          return 'kolom pencarian tidak boleh kosong';
                        }
                        bukuJudul = bukuJudulvalue;
                        return null;
                      },
                    ),
                    const SizedBox(height: 18),
                    TextButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 18, vertical: 10),
                        child: Text(
                          _isLoading ? 'Pencarian Buku' : 'Cari Buku',
                          textDirection: TextDirection.ltr,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          //BookPage(bukuJudul: bukuJudul.toString());
                          _proses_cari(bukuJudul);
                        }
                      },
                    ),
                  ])),
        )
      ],
    );
  }

  void _proses_cari(String? data) async {
    setState(() {
      _isLoading = true;
    });
    String? judul = bukuJudul;
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => BookDashboardPage(bukuJudul: judul.toString())));
    _showMsg("Proses pencarian berhasil dilakukan.");
  }

  // Daftar Buku ========================================================
  Widget _daftarBuku() {
    return FutureBuilder(
      future: _bookService,
      builder: (BuildContext context, AsyncSnapshot<List<BookModel>> snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(
                "Something wrong with message: ${snapshot.error.toString()}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          List<BookModel> books = snapshot.data!;
          return _buildListView(books);
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _buildListView(List<BookModel> listBuku) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: listBuku.length,
          itemBuilder: (context, index) {
            List<BookModel> book = listBuku;
            return Container(
              margin: const EdgeInsets.all(1.0),
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Card(
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2.0))),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ReadPage(
                                bukuId: book[index].bukuId.toString())));
                      },
                      customBorder: const CircleBorder(),
                      child: Column(
                        children: <Widget>[
                          const Padding(padding: EdgeInsets.only(top: 10.0)),
                          ClipRRect(
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(18.0),
                              topRight: Radius.circular(18.0),
                            ),
                            child: Image.network(
                                '$urlImageBuku${book[index].bukuCover.toString()}',
                                width: 150,
                                height: 250,
                                fit: BoxFit.cover),
                          ),
                          ListTile(
                            title: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: Text(book[index].bukuJudul.toString()),
                              ),
                            ),
                            subtitle: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: Text(book[index].penulisNama.toString()),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ]),
            );
          }),
    );
  }

  // Pencarian ==========================================================

  // ====================================================================
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          _pencarianBuku(),
          _daftarBuku(),
        ],
      )),
    );
  }
}
