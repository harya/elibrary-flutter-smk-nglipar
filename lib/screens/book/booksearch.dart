import 'package:elibrary_master/screens/book/bookmain.dart';
import 'package:elibrary_master/screens/home/home.dart';
import 'package:elibrary_master/screens/pengaturan/setting.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:elibrary_master/model/bookModel.dart';
import 'package:elibrary_master/services/bookService.dart';
import 'package:elibrary_master/constant/url.dart';
import 'package:elibrary_master/screens/book/read.dart';
import 'package:elibrary_master/style/color_lib.dart';
import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';

class BookSearch extends StatefulWidget {
  const BookSearch({super.key, this.bukuJudul});
  final String? bukuJudul;

  @override
  _BookSearchState createState() => _BookSearchState();
}

class _BookSearchState extends State<BookSearch> {
  // Delare ===============================================
  final urlImageBuku = imgBukuUrl;
  bool _isLoading = false;
  var bukuJudul;
  Future<List<BookModel>>? _bookService;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _secureText = true;
  Widget? _child;
  //=======================================================
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg!),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    String? bukuJudul = widget.bukuJudul.toString();
    _child = BookSearch(bukuJudul: bukuJudul);
    super.initState();
    _bookService = BookService.getBookSearch(widget.bukuJudul.toString());
  }

  //Pencarian Buku ========================================
  Widget _pencarianBuku() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(24),
          key: _scaffoldKey,
          child: Form(
              key: _formKey,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextFormField(
                      cursorColor: ColorLib.blueLight,
                      keyboardType: TextInputType.text,
                      decoration: const InputDecoration(
                        hintText: "Masukan Judul buku yang dicari",
                      ),
                      validator: (bukuJudulvalue) {
                        if (bukuJudulvalue == '') {
                          return 'kolom pencarian masih kosong';
                        }
                        bukuJudul = bukuJudulvalue;
                        return null;
                      },
                    ),
                    const SizedBox(height: 18),
                    TextButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 18, vertical: 10),
                        child: Text(
                          _isLoading ? 'Pencarian Buku' : 'Cari Buku',
                          textDirection: TextDirection.ltr,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          //BookPage(bukuJudul: bukuJudul.toString());
                          _proses_cari(bukuJudul);
                        } else {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => BookMain()));
                          _showMsg("Proses pencarian berhasil dilakukan.");
                        }
                      },
                    ),
                  ])),
        )
      ],
    );
  }

  void _proses_cari(String? data) async {
    setState(() {
      _isLoading = true;
    });
    String? judul = bukuJudul;
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => BookSearch(bukuJudul: judul.toString())));
    _showMsg("Proses pencarian berhasil dilakukan.");
  }

  //Daftar Buku ===========================================
  Widget _daftarBuku() {
    return FutureBuilder(
      future: _bookService,
      builder: (BuildContext context, AsyncSnapshot<List<BookModel>> snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(
                "Something wrong with message: ${snapshot.error.toString()}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          List<BookModel> books = snapshot.data!;
          return _buildListView(books);
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _buildListView(List<BookModel> listBuku) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: listBuku.length,
          itemBuilder: (context, index) {
            List<BookModel> book = listBuku;
            return Container(
              margin: const EdgeInsets.all(1.0),
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Card(
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2.0))),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ReadPage(
                                bukuId: book[index].bukuId.toString())));
                      },
                      customBorder: const CircleBorder(),
                      child: Column(
                        children: <Widget>[
                          const Padding(padding: EdgeInsets.only(top: 10.0)),
                          ClipRRect(
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(18.0),
                              topRight: Radius.circular(18.0),
                            ),
                            child: Image.network(
                                '$urlImageBuku${book[index].bukuCover.toString()}',
                                width: 150,
                                height: 250,
                                fit: BoxFit.cover),
                          ),
                          ListTile(
                            title: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: Text(book[index].bukuJudul.toString()),
                              ),
                            ),
                            subtitle: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: Text(book[index].penulisNama.toString()),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ]),
            );
          }),
    );
  }

  //=======================================================
  Widget buildView() {
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        _pencarianBuku(),
        _daftarBuku(),
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          _pencarianBuku(),
          _daftarBuku(),
        ],
      )),
    );
  }
}
