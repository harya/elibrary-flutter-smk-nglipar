import 'package:flutter/material.dart';
import 'package:elibrary_master/screens/book/book.dart';
import 'package:elibrary_master/screens/pengaturan/setting.dart';
import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';
import 'package:elibrary_master/screens/home/top_home.dart';
import 'package:elibrary_master/screens/home/home_list_baca.dart';
import 'package:elibrary_master/style/color_lib.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: <Widget>[topWidget(), HomeListBaca()],
    )));
  }
}
