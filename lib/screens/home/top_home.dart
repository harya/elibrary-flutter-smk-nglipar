import 'package:elibrary_master/style/color_lib.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elibrary_master/constant/url.dart';
import 'dart:core';


class topWidget extends StatefulWidget {
  const topWidget({super.key});

  @override
  _topWidget createState() => _topWidget();
}

class _topWidget extends State<topWidget> {
  String angkatan = "";
  String angkatanSiswa = "";
  String name = "";
  String no_nis = "";
  String nis = "";
  String fotoSiswa = "";
  String urlImage = "";


  @override
  void initState() {
    super.initState();
    getName();
  }

  getName() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    late String? user = localStorage.getString('user').toString();
    late String? nis = localStorage.getString('nis').toString();
    late String? angkatan = localStorage.getString('angkatan').toString();
    late String? foto =
        localStorage.getString('foto_siswa').toString().replaceAll('"', '');
    late String? urlImage = imgProfUrl;

    setState(() {
      angkatanSiswa = angkatan.replaceAll('"', '');
      name = user.replaceAll('"', '');
      no_nis = nis.replaceAll('"', '');
      fotoSiswa = '$urlImage$foto';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:  EdgeInsets.all(10),
      decoration:  BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        color: ColorLib.colorColl01,
      ),
      padding:  EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: Padding(
        padding:  EdgeInsets.all(30.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CircleAvatar(
              radius: 40,
              backgroundImage: NetworkImage(fotoSiswa),
            ),
            const Padding(padding: EdgeInsets.only(top: 10, left: 10.0)),
            Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    name,
                    style: const TextStyle(
                        color: ColorLib.textColor01,
                        fontSize: 19.0,
                        fontWeight: FontWeight.bold),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 10.0)),
                  Text(
                    'NIS : $no_nis',
                    style: const TextStyle(
                        color: ColorLib.textColor02, fontSize: 14.0),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 5.0)),
                  Text(
                    'Angkatan : $angkatanSiswa',
                    style: const TextStyle(
                        color: ColorLib.textColor02,
                        fontStyle: FontStyle.italic,
                        fontSize: 12.0),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),

    );
  }

}
