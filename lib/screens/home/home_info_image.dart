import 'package:flutter/material.dart';

class HomeInfoImage extends StatelessWidget {
  const HomeInfoImage({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        FadeInImage(
          alignment: Alignment.topCenter,
          placeholder: const AssetImage("assets/profils/weather.png"),
          image: Image.asset("assets/profils/weather.png").image,
          fit: BoxFit.fill,
          height: 100,
        )
      ],
    );
  }
}
