import 'package:elibrary_master/style/color_lib.dart';
import 'package:flutter/material.dart';
//import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class ProfileTopWidget extends StatelessWidget {
  const ProfileTopWidget({super.key});

  Future<String> getNamaUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var namaPengguna = localStorage != '' ? localStorage.getString('user') : '';

    return namaPengguna.toString();
  }

  @override
  Widget build(BuildContext context) {
    Future<String> infopengguna() async {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      return localStorage.getString('user').toString();
    }

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: ColorLib.colorColl01,
      ),
      padding: const EdgeInsets.all(15.0),
      margin: const EdgeInsets.all(8.0),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            radius: 40,
            backgroundImage:
                ExactAssetImage('assets/profils/Sriharyo_Foto.png'),
          ),
          Padding(padding: EdgeInsets.only(top: 10, left: 10.0)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Selamat Datang",
                style: TextStyle(
                    color: ColorLib.textColor01,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              Text(
                'nama',
                style: TextStyle(color: ColorLib.textColor02, fontSize: 14.0),
              )
            ],
          ),
        ],
      ),
    );
  }
}
