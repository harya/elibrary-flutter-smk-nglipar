// import 'package:elibrary_master/screens/home/top_home.dart';
import 'package:flutter/material.dart';
import 'package:elibrary_master/model/bacaModel.dart';
import 'package:elibrary_master/services/bacaService.dart';
import 'package:elibrary_master/constant/url.dart';
// import 'package:http/http.dart' as http;
// import 'dart:typed_data';

class HomeListBaca extends StatefulWidget {
  const HomeListBaca({super.key});
  @override
  _HomeListBacaState createState() => _HomeListBacaState();
}

class _HomeListBacaState extends State<HomeListBaca> {
  final urlImageBuku = imgBukuUrl;
  bool _isLoading = false;
  bool _secureText = true;
  Future<List<BacaModel>>? _bacaService;
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg!),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    _bacaService = BacaService.getBaca();
  }

  Widget _buildListUserBaca(List<BacaModel> listBaca) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: listBaca.length,
          itemBuilder: (context, index) {
            List<BacaModel> bacaBuku = listBaca;
            // String? _file_url =imgBukuUrl + bacaBuku[index].bukuFile.toString();

            return Container(
              decoration: myBoxDecoration(),
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(top: 10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.all(5.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(4.0)),
                          child: Image.network(
                              '$urlImageBuku${bacaBuku[index].bukuCover.toString()}',
                              width: 50,
                              height: 80,
                              fit: BoxFit.cover),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(left: 10.0)),
                    Row(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              bacaBuku[index].bukuJudul.toString(),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 4.0)),
                            Text(
                              'Dibaca sebanyak ' +
                                  bacaBuku[index].jumlahBuku.toString() +
                                  ' kali',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 4.0)),
                            Text(
                              bacaBuku[index].penulisNama.toString(),
                              style: TextStyle(fontSize: 10),
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 4.0)),
                            Text(
                              bacaBuku[index].penerbitNama.toString(),
                              style: TextStyle(fontSize: 10),
                            ),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
              padding: EdgeInsets.only(top: 5.0, bottom: 10.0),
            );
          }),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
        color: Colors.black45, //                   <--- border color
        width: 2.0,
      ),
      borderRadius: const BorderRadius.all(Radius.circular(5.0)),
    );
  }

  Widget _daftarBaca() {
    return FutureBuilder(
        future: _bacaService,
        builder:
            (BuildContext context, AsyncSnapshot<List<BacaModel>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                  "Something wrong with message: ${snapshot.error.toString()}"),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            List<BacaModel> bacas = snapshot.data!;
            return _buildListUserBaca(bacas);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[_daftarBaca()],
    );
  }
}
