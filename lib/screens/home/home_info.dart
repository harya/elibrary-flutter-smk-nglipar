// import 'package:elibrary_master/style/color_lib.dart';
import 'package:elibrary_master/screens/home/home_info_image.dart';

import 'package:flutter/material.dart';

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({super.key});

  Future<void> _loadUserData() async {}

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20.0),
      margin: const EdgeInsets.all(8.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(25.0),
          ),
          color: Theme.of(context).primaryColor),
      height: 650,
      child: const Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                HomeInfoImage(),
                Padding(padding: EdgeInsets.only(left: 15)),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 15.0)),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                HomeInfoImage(),
                Padding(padding: EdgeInsets.only(left: 15)),
                HomeInfoImage(),
                Padding(padding: EdgeInsets.only(left: 15)),
                HomeInfoImage(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
