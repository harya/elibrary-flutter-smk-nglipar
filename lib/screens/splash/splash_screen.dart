import 'dart:async';
import 'package:elibrary_master/style/color_lib.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var duration = const Duration(seconds: 3);

    return Timer(duration, route);
  }

  route() {
    Navigator.pushReplacementNamed(context, '/CheckAuth');
  }

  @override
  Widget build(BuildContext context) {
    return initWidget(context);
  }

  Widget initWidget(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  ColorLib.blueAccent,
                  ColorLib.blueLight,
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
          Center(
            child: SizedBox(
              height: size.height * 0.4,
              width: size.width * 0.5,
              child: Image.asset('assets/images/books.png'),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 16),
            alignment: Alignment.bottomCenter,
            child: Text(
              'E-Library Mobile App',
              style: const TextStyle(
                      fontWeight: FontWeight.w500, color: Colors.white)
                  .copyWith(fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }
}
