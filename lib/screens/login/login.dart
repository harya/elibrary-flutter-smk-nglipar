import 'dart:convert';
import 'dart:io';
import 'package:elibrary_master/screens/dasboard/dashboard.dart';
import 'package:flutter/services.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:elibrary_master/screens/register/register.dart';
import 'package:elibrary_master/style/color_lib.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elibrary_master/services/authentication/http_services.dart';
import 'package:flutter/foundation.dart';

import 'dart:developer' as developer;

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final NetworkInfo _networkInfo = NetworkInfo();
  var email, password;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _secureText = true;
  String _connectionStatus = 'Unknown';

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg!),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: ColorLib.whiteColor,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 72),
          child: Column(
            children: [
              Card(
                elevation: 4.0,
                color: ColorLib.blueLight,
                margin: const EdgeInsets.only(top: 86),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(24),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "Login",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: ColorLib.whiteColor,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 18),
                        TextFormField(
                            cursorColor: ColorLib.blueLight,
                            keyboardType: TextInputType.text,
                            decoration: const InputDecoration(
                              hintText: "Email",
                            ),
                            validator: (emailValue) {
                              if (emailValue == '') {
                                return 'Please enter your email';
                              }
                              email = emailValue;
                              return null;
                            }),
                        const SizedBox(height: 10),
                        TextFormField(
                            cursorColor: ColorLib.blueAccent,
                            keyboardType: TextInputType.text,
                            obscureText: _secureText,
                            decoration: InputDecoration(
                              hintText: "Password",
                              suffixIcon: IconButton(
                                onPressed: showHide,
                                icon: Icon(_secureText
                                    ? Icons.visibility_off
                                    : Icons.visibility),
                              ),
                            ),
                            validator: (passwordValue) {
                              if (passwordValue!.isEmpty) {
                                return 'Please enter your password';
                              }
                              password = passwordValue;
                              return null;
                            }),
                        const SizedBox(height: 12),
                        TextButton(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 18, vertical: 10),
                            child: Text(
                              _isLoading ? 'Proccessing..' : 'Login',
                              textDirection: TextDirection.ltr,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                decoration: TextDecoration.none,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _login();
                            }
                          },
                        ),
                        Text(_connectionStatus),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 24,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Does'nt have an account? ",
                    style: TextStyle(
                      color: ColorLib.textColor02,
                      fontSize: 16,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegisterPage()));
                    },
                    child: const Text(
                      'Register',
                      style: TextStyle(
                        color: ColorLib.textColor01,
                        fontSize: 16.0,
                        decoration: TextDecoration.none,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void _login() async {
    setState(() {
      _isLoading = true;
    });
    String? wifiGatewayIP;
    if (!kIsWeb && (Platform.isWindows || Platform.isLinux)) {
      debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
    }
    try {
      if (!Platform.isWindows) {
        wifiGatewayIP = await _networkInfo.getWifiGatewayIP();
      }
    } on PlatformException catch (e) {
      developer.log('Failed to get Wifi gateway address', error: e);
      wifiGatewayIP = 'Failed to get Wifi gateway address';
    }
    var data = {'email': email, 'password': password,'gateway':wifiGatewayIP};
    var response = await HttpServices.postData(data, '/login');

    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('group', json.encode(body['group']));
      localStorage.setString('token', json.encode(body['token']));
      localStorage.setString('angkatan', json.encode(body['angkatan']));
      localStorage.setString('nis', json.encode(body['nis']));
      localStorage.setString('id_user', json.encode(body['id_user']));
      localStorage.setString('user', json.encode(body['nama_lengkap']));
      localStorage.setString('auth', json.encode(body['auth']));
      localStorage.setString('foto_siswa', json.encode(body['siswa_foto']));
      localStorage.setString('email', json.encode(body['email']));
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => const DashboardPage()),
      );
      _showMsg("Proses login berhasil dilakukan.");
    } else {
      _showMsg("Proses login gagal dilakukan, silahkan lakukan login ulang.");
    }

    setState(() {
      _connectionStatus = 'Wifi GateWay : $wifiGatewayIP\n';
      _isLoading = false;
    });
  }
}
