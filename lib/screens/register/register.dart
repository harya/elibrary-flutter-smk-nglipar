import 'package:elibrary_master/screens/login/login.dart';
import 'package:flutter/material.dart';
import 'package:elibrary_master/style/color_lib.dart';
import 'package:elibrary_master/services/authentication/http_services.dart';
import 'dart:convert';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  var email, password, repassword;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _secureText = true;
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg!),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorLib.whiteColor,
        key: _scaffoldKey,
        body: SafeArea(
            child: SingleChildScrollView(
                padding:
                    const EdgeInsets.symmetric(horizontal: 28, vertical: 72),
                child: Column(children: [
                  Card(
                    elevation: 4.0,
                    color: ColorLib.blueLight,
                    margin: const EdgeInsets.only(top: 86),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(24),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // =================================================================
                            const Text(
                              "Daftar Akun",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: ColorLib.whiteColor,
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            // =================================================================
                            const SizedBox(height: 18),
                            // =================================================================
                            TextFormField(
                                cursorColor: ColorLib.blueLight,
                                keyboardType: TextInputType.text,
                                decoration: const InputDecoration(
                                  hintText: "Masukan e-mail ",
                                ),
                                validator: (emailValue) {
                                  if (emailValue == '') {
                                    return 'E - mail tidak boleh kosong';
                                  }
                                  email = emailValue;
                                  return null;
                                }),
                            // =================================================================
                            const SizedBox(height: 10),
                            // =================================================================
                            TextFormField(
                                cursorColor: ColorLib.blueAccent,
                                keyboardType: TextInputType.text,
                                obscureText: _secureText,
                                decoration: InputDecoration(
                                  hintText: "Masukan password",
                                  suffixIcon: IconButton(
                                    onPressed: showHide,
                                    icon: Icon(_secureText
                                        ? Icons.visibility_off
                                        : Icons.visibility),
                                  ),
                                ),
                                validator: (passwordValue) {
                                  if (passwordValue!.isEmpty) {
                                    return 'Password tidak boleh kosong ... !';
                                  }
                                  password = passwordValue;
                                  return null;
                                }),
                            // =================================================================
                            const SizedBox(height: 10),
                            // =================================================================
                            TextFormField(
                                cursorColor: ColorLib.blueAccent,
                                keyboardType: TextInputType.text,
                                obscureText: _secureText,
                                decoration: InputDecoration(
                                  hintText: "Ulangi password",
                                  suffixIcon: IconButton(
                                    onPressed: showHide,
                                    icon: Icon(_secureText
                                        ? Icons.visibility_off
                                        : Icons.visibility),
                                  ),
                                ),
                                validator: (repasswordValue) {
                                  if (repasswordValue!.isEmpty) {
                                    return 'Pengulangan password tidak boleh kosong ... !';
                                  }
                                  repassword = repasswordValue;
                                  return null;
                                }),
                            // =================================================================
                            const SizedBox(height: 12),
                            // =================================================================
                            TextButton(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18, vertical: 10),
                                child: Text(
                                  _isLoading
                                      ? 'Proccessing Registrasi..'
                                      : 'Daftar',
                                  textDirection: TextDirection.ltr,
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 18.0,
                                    decoration: TextDecoration.none,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  _registrasi();
                                }
                              },
                            ),
                            // =================================================================
                          ],
                        ),
                      ),
                    ),
                  )
                ]))));
  }

  void _registrasi() async {
    setState(() {
      _isLoading = true;
    });
    var data = {'email': email, 'password': password, 'cpassword': repassword};
    var response = await HttpServices.postData(data, '/register');
    if (response.statusCode == 200) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => const LoginPage()),
      );
      _showMsg("Proses register berhasil dilakukan.");
    } else {
      _showMsg(
          "Proses register gagal dilakukan, silahkan lakukan pendaftaran ulang.");
    }
    setState(() {
      _isLoading = false;
    });
  }
}
