import 'package:elibrary_master/style/color_lib.dart';
import 'package:flutter/material.dart';

class ProfileWidget extends StatelessWidget {
  const ProfileWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(
                '1192881772819',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  letterSpacing: 0.2,
                  color: ColorLib.textColor01,
                ),
              ),
              Text(
                'Sriharyo Nurcahyo Kusumo',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  letterSpacing: 0.27,
                  color: ColorLib.textColor01,
                ),
              ),
            ],
          ),
        ],
      ),
      Padding(
          padding: EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 20)),
    ]);
  }
}
