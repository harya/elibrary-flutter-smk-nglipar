import 'package:flutter/material.dart';
import 'package:elibrary_master/style/color_lib.dart';

class FluidNavBarPage extends StatefulWidget {
  @override
  _FluidNavBarState createState() => _FluidNavBarState();
}

class _FluidNavBarState extends State<FluidNavBarPage> {
  int _selectedIndex = 0;
  List<NavItem> _navItems = [
    NavItem(Icons.home, "Home"),
    NavItem(Icons.book_sharp, "Buku"),
    NavItem(Icons.settings, "Profil"),
  ];
  void _onNavItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.home_filled),
      ),
      bottomNavigationBar: BottomAppBar(
        color: ColorLib.bgStyleAppBar,
        shape: const CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: _navItems.map((item) {
            var index = _navItems.indexOf(item);
            return IconButton(
              onPressed: () => _onNavItemTapped(index),
              icon: Icon(
                item.icon,
                color: _selectedIndex == index
                    ? ColorLib.hintColor
                    : ColorLib.bgStyleOne,
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}

class NavItem {
  IconData icon;
  String title;

  NavItem(this.icon, this.title);
}
