import 'dart:convert';
import 'dart:io';
import 'package:elibrary_master/constant/url.dart';
import 'package:elibrary_master/model/bukusModel.dart';
import 'package:elibrary_master/services/token.dart';
import 'package:http/http.dart' as http;

class BukuServices {
  static Future<List<Bukus>> getBuku() async {
    try {
      await getToken();
      String fullUrl = bukuUrl;
      final response =
          await http.get(Uri.parse(fullUrl), headers: setHeaders());

      if (response.statusCode.toString() == "200") {
        Map<String, dynamic> map = json.decode(response.body)['bukus'];
        List<Bukus> orderBukussList;
        orderBukussList =
            List<Bukus>.from(map["bukus"].map((x) => Bukus.fromJson(x)));
        return orderBukussList;
      } else {
        return Future.error('Gagal Memuat Data buku :(');
      }
    } on SocketException {
      return Future.error('Tidak Ada Koneksi Internet :(');
    } on HttpException {
      return Future.error('Layanan Tidak Ditemukan :(');
    } on FormatException {
      return Future.error('Format Data Tidak Valid :(');
    } catch (e) {
      return Future.error(e.toString());
    }
  }
}
