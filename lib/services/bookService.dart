import 'dart:convert';
import 'dart:io';
//import 'package:elibrary_master/constant/url.dart';
import 'package:elibrary_master/model/bookModel.dart';
import 'package:elibrary_master/services/token.dart';
import 'package:elibrary_master/services/authentication/http_services.dart';
// import 'package:http/http.dart' as http;

class BookService {
  static Future<List<BookModel>> getBook() async {
    try {
      await getToken();
      final response = await HttpServices.getData('/bukus');
      if (response.statusCode == 200) {
        List jsonBukus = json.decode(response.body);
        return jsonBukus.map((bukus) => BookModel.fromJson(bukus)).toList();
      } else {
        return Future.error('Gagal Memuat Data buku!');
      }
    } on SocketException {
      return Future.error('Tidak Ada Koneksi Internet !');
    } on HttpException {
      return Future.error('Layanan Tidak Ditemukan !');
    } on FormatException {
      return Future.error('Format Data Tidak Valid :(');
    } catch (e) {
      return Future.error(e.toString());
    }
  }

  static Future<List<BookModel>> getBookSearch(search) async {
    try {
      await getToken();
      var data = {'search': search};

      final response = await HttpServices.postData(data, '/bukus/search');

      if (response.statusCode == 200) {
        List jsonBukus = json.decode(response.body);
        return jsonBukus.map((bukus) => BookModel.fromJson(bukus)).toList();
      } else {
        return Future.error('Gagal Memuat Data buku!');
      }
    } on SocketException {
      return Future.error('Tidak Ada Koneksi Internet !');
    } on HttpException {
      return Future.error('Layanan Tidak Ditemukan !');
    } on FormatException {
      return Future.error('Format Data Tidak Valid :(');
    } catch (e) {
      return Future.error(e.toString());
    }
  }
}
