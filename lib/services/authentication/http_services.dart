import 'dart:convert';
import 'package:elibrary_master/services/token.dart';
import 'package:elibrary_master/constant/url.dart';
import 'package:http/http.dart' as http;

class HttpServices {
  static postData(data, String postUrl) async {
    var fullUrl = apiUrl + postUrl;
    return await http.post(Uri.parse(fullUrl),
        body: jsonEncode(data), headers: setHeaders());
  }

  static getData(String getUrl) async {
    var fullUrl = apiUrl + getUrl;
    await getToken();
    return await http.get(Uri.parse(fullUrl), headers: setHeaders());
  }

  static putData(data, String putUrl, String id) async {
    var fullUrl = apiUrl + putUrl + id;
    await getToken();
    return await http.put(Uri.parse(fullUrl),
        body: jsonEncode(data), headers: setHeaders());
  }

  static getFile(String strUrl) async {
    var fullUrl = pdfBukuUrl + strUrl;
    await getToken();
    
  }
}
