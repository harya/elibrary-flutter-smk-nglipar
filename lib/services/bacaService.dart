import 'dart:convert';
import 'dart:io';
import 'package:elibrary_master/constant/url.dart';
import 'package:elibrary_master/model/bacaModel.dart';
import 'package:elibrary_master/services/token.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class BacaService {
  static Future<List<BacaModel>> getBaca() async {
    try {
      await getToken();
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var id_user = localStorage != '' ? localStorage.getString('id_user') : '';
      String baseUrl = apiUrl + '/bukus/' + id_user.toString() + '/readUser/';
      final response =
          await http.get(Uri.parse(baseUrl), headers: setHeaders());

      if (response.statusCode == 200) {
        List jsonBacas = json.decode(response.body);
        return jsonBacas.map((bacas) => BacaModel.fromJson(bacas)).toList();
      } else {
        return Future.error('Belum pernah membaca buku!');
      }
    } on SocketException {
      return Future.error('Tidak Ada Koneksi Internet !');
    } on HttpException {
      return Future.error('Layanan Tidak Ditemukan !');
    } on FormatException {
      return Future.error('Format Data Tidak Valid :(');
    } catch (e) {
      return Future.error(e.toString());
    }
  }
}
