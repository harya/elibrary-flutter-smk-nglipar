//* Backend Url
const baseUrl = 'http://172.26.224.1:8000';

const apiUrl = '$baseUrl/api';

const bukuUrlSearch = '$apiUrl/bukus/search/';
const bukuUrl = '$apiUrl/bukus/';

//* Base File URL
const fileUrl = '172.26.224.1/elib-api/public';
//* Images Url
const imgBukuUrl = '$baseUrl/books/images/';
const imgProfUrl = '$baseUrl/images/profil/';
//* File PDF
const pdfBukuUrl = '172.26.224.1/elib-api/public/books/pdf/';
//* Office apps embedding untuk webview
const openDocumentUrl = 'https://view.officeapps.live.com/op/view.aspx?src=';
