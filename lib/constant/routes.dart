import 'package:elibrary_master/screens/register/register.dart';
import 'package:elibrary_master/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:elibrary_master/services/authentication/auth.dart';
import 'package:elibrary_master/screens/login/login.dart';
import 'package:elibrary_master/screens/dasboard/dashboard.dart';
import 'package:elibrary_master/screens/home/home.dart';
import 'package:elibrary_master/screens/buku/buku.dart';
import 'package:elibrary_master/screens/book/book.dart';

class RouteGenerator {
  static MaterialPageRoute? generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    //* Fungsi Cek Autentikasi User
    switch (settings.name) {
      case '/CheckAuth':
        return MaterialPageRoute(
            builder: (BuildContext context) => const CheckAuth());
      case '/loginPage':
        return MaterialPageRoute(
            builder: (BuildContext context) => const LoginPage());
      case '/DashboardPage':
        return MaterialPageRoute(
            builder: (BuildContext context) => const DashboardPage());
      case '/HomePage':
        return MaterialPageRoute(
            builder: (BuildContext context) => const HomePage());
      case '/BukuPage':
        return MaterialPageRoute(
            builder: (BuildContext context) => const BukuPage());
      case '/BookPage':
        return MaterialPageRoute(
            builder: (BuildContext context) => const BookPage());
      case '/RegisterPage':
        return MaterialPageRoute(
            builder: (BuildContext context) => const RegisterPage());
      case '/SplashScreen':
        return MaterialPageRoute(
            builder: (BuildContext context) => const SplashScreen());
      default:
        return null;
    }
  }
}
