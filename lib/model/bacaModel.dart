import 'dart:convert';

class BacaModel {
  String? bukuId;
  String? bukuJudul;
  String? penerbitNama;
  String? penulisNama;
  String? bukuCover;
  String? bukuFile;
  int? jumlahBuku;

  BacaModel({
    required this.bukuId,
    required this.bukuJudul,
    required this.penerbitNama,
    required this.penulisNama,
    required this.bukuCover,
    required this.bukuFile,
    required this.jumlahBuku,
  });

  factory BacaModel.fromRawJson(String str) =>
      BacaModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BacaModel.fromJson(Map<String, dynamic> json) => BacaModel(
        bukuId: json["buku_id"],
        bukuJudul: json["buku_judul"],
        penerbitNama: json["penerbit_nama"],
        penulisNama: json["penulis_nama"],
        bukuCover: json["buku_cover"],
        bukuFile: json["buku_file"],
        jumlahBuku: json["jumlah_buku"],
      );
  Map<String, dynamic> toJson() => {
        "buku_id": bukuId,
        "buku_judul": bukuJudul,
        "penerbit_nama": penerbitNama,
        "penulis_nama": penulisNama,
        "buku_cover": bukuCover,
        "buku_file": bukuFile,
        "jumlah_buku": jumlahBuku
      };
}
