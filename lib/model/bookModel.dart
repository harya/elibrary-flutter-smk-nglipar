import 'dart:convert';

class BookModel {
  String bukuId;
  String? bukuJudul;
  String bukuCover;
  String bukuIsbn;
  String bukuFile;
  int bukuTahun;
  String? penulisId;
  String? penulisNama;
  String? penerbitId;
  String? penerbitNama;
  String? penerbitAlamat;
  String? penerbitEmail;
  DateTime createdAt;
  DateTime updatedAt;

  BookModel({
    required this.bukuId,
    required this.bukuJudul,
    required this.bukuCover,
    required this.bukuIsbn,
    required this.bukuFile,
    required this.bukuTahun,
    required this.penulisId,
    required this.penulisNama,
    required this.penerbitId,
    required this.penerbitNama,
    required this.penerbitAlamat,
    required this.penerbitEmail,
    required this.createdAt,
    required this.updatedAt,
  });

  factory BookModel.fromRawJson(String str) =>
      BookModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BookModel.fromJson(Map<String, dynamic> json) => BookModel(
        bukuId: json["buku_id"],
        bukuJudul: json["buku_judul"],
        bukuCover: json["buku_cover"],
        bukuIsbn: json["buku_isbn"],
        bukuFile: json["buku_file"],
        bukuTahun: json["buku_tahun"],
        penulisId: json["penulis_id"],
        penulisNama: json["penulis_nama"],
        penerbitId: json["penerbit_id"],
        penerbitNama: json["penerbit_nama"],
        penerbitAlamat: json["penerbit_alamat"],
        penerbitEmail: json["penerbit_email"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "buku_id": bukuId,
        "buku_judul": bukuJudul,
        "buku_cover": bukuCover,
        "buku_isbn": bukuIsbn,
        "buku_file": bukuFile,
        "buku_tahun": bukuTahun,
        "penulis_id": penulisId,
        "penulis_nama": penulisNama,
        "penerbit_id": penerbitId,
        "penerbit_nama": penerbitNama,
        "penerbit_alamat": penerbitAlamat,
        "penerbit_email": penerbitEmail,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
